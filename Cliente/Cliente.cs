﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Modelo.Cliente
{
    public class Cliente : Model
    {
        public string NombreComercial
        {
            get;
            set;
        }
        [MaxLength(13), RegularExpression("^([A-ZÑ&]{3,4})([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|[12][0-9]|3[01])([A-Z[0-9]]{2})([A[0-9]])$", ErrorMessage = "Revise el RFC, no cumple con la estructura requerida")]
        public string RFC
        {
            get;
            set;
        }

        public bool EsPersonaMoral
        {
            get;
            set;
        }
        public Persona.Fisica Contacto
        {
            get;
            set;
        }
        [DisplayName("Dirección")]
        public int? DireccionId
        {
            get;
            set;
        }
        public Persona.Direccion Direccion
        { 
            get;
            set;
        }

        public ICollection<Servicio.Contrato> Contratos
        {
            get;
            set;
        }


    }
}
