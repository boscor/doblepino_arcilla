﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Modelo.Persona
{
    public class Cooperativista : Fisica
    {
        public bool EsSocio
        {
            get;
            set;
        }

        public ICollection<Servicio.BitacoraCooperativista> Bitacoras
        {
            get;
            set;
        }



    }
}
