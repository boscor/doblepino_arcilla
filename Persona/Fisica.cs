﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Modelo.Persona
{
    public class Fisica : Model
    {
        [Required, DisplayName("Apellido paterno")]
        public string ApellidoPaterno
        {
            get;
            set;
        }
        [Required, DisplayName("Apellido materno")]
        public string ApellidoMaterno
        {
            get;
            set;
        }

        [Required, RegularExpression("^([A-Z&]{4})([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|[12][0-9]|3[01])([MH][A-Z]{2})([A-Z&]{3})([A-Z0][0-9])$", ErrorMessage = "Revise el CURP, no cumple con la estructura requerida")]
        public string CURP
        {
            get;
            set;
        }
        [Required, DisplayName("Correo electrónico"), RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Revise el email, no cumple con la estructura requerida")]
        public string Email
        {
            get;
            set;
        }
        [Required, DisplayName("Número de celular"), RegularExpression(@"^([\d]{10})$", ErrorMessage = "Revise el teléfono, debe ser de 10 dígitos")]
        
        public string TelefonoMovil
        {
            get;
            set;
        }
        [DisplayName("Dirección")]
        public int? DireccionId
        {
            get;
            set;
        }

        public Persona.Direccion Direccion
        {
            get;
            set;
        }
    }
}
