﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Modelo.Persona
{
    public class Direccion
    {
        public int Id
        { get; set; }
        [DisplayName("Teléfono fijo")]
        public string TelefonoFijo
        {
            get;
            set;
        }

        [Required]
        public string Calle
        {
            get;
            set;
        }
        [Required, DisplayName("Número exterior")]
        public string NumeroExterior
        {
            get;
            set;
        }
        [DisplayName("Número interior")]
        public string NumeroInterior
        { 
            get;
            set;
        }
        [Required]
        public string Colonia
        {
            get;
            set;
        }
        public string Localidad
        {
            get;
            set;
        }
        [Required]
        public string Municipio
        {
            get;
            set;
        }

        public enum EEstado
        {
            Aguascalientes = 1,
            Baja_California = 2,
            Baja_California_Sur = 3,
            Campeche = 4,
            Coahuila_de_Zaragoza = 5,
            Colima = 6,
            Chiapas = 7,
            Chihuahua = 8,
            Ciudad_de_México = 9,
            Durango = 10,
            Guanajuato = 11,
            Guerrero = 12,
            Hidalgo = 13,
            Jalisco = 14,
            México = 15,
            Michoacán_de_Ocampo = 16,
            Morelos = 17,
            Nayarit = 18,
            Nuevo_León = 19,
            Oaxaca = 20,
            Puebla = 21,
            Querétaro = 22,
            Quintana_Roo = 23,
            San_Luis_Potosí = 24,
            Sinaloa = 25,
            Sonora = 26,
            Tabasco = 27,
            Tamaulipas = 28,
            Tlaxcala = 29,
            Veracruz_de_Ignacio_de_la_Llave = 30,
            Yucatán = 31,
            Zacatecas = 32
        }
        public EEstado Estado
        {
            get;
            set;
        }
        [Required, DisplayName("Código postal")]
        public string CP
        {
            get;
            set;
        }

        public Fisica Fisica
        { get; set; }

        public Cliente.Cliente Cliente
        { get; set; }

        public override string ToString()
        {
            return Calle + " #" + NumeroExterior;
        }

    }
}
