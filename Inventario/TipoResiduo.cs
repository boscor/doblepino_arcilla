﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Modelo.Inventario
{
    public class TipoResiduo : Model
    {
        public int? PadreId
        {
            get;
            set;
        }
        public TipoResiduo Padre
        {
            get;
            set;
        }
    }
}
