﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Modelo.Inventario
{
    public class Almacen : Model
    {
        public int EncargadoId
        {
            get;
            set;
        }

        public Persona.Cooperativista Encargado
        { 
            get;
            set;
        }

        public ICollection<ResiduoAlmacen> ResiduosEnAlmacen
        {
            get;
            set;
        }

    }
}
