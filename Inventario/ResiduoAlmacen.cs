﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Modelo.Inventario
{
    public class ResiduoAlmacen
    {
        public int Id
        { get; set; }
        public int AlmacenId
        { get; set; }
        public Almacen Almacen
        { get; set; }
        public int ResiduoId
        { get; set; }
        public Residuo Residuo
        { get; set;}
        public decimal Existencia
        { get; set; }
        public int UnidadMedidaId
        { get; set; }
        public UnidadMedida UnidadMedida
        { get; set; }
    }
}
