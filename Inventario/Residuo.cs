﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Modelo.Inventario
{
    public class Residuo : Model
    {
        [Required]
        public string Clasificacion
        {
            get;
            set;
        }


        public int TipoResiduoId
        {
            get;
            set;
        }
        public TipoResiduo TipoResiduo
        {
            get;
            set;
        }

        public bool SeEnsucia
        { 
            get;
            set;
        }

        public bool SeHumedece
        {
            get;
            set;
        }

        public bool SeEmpolva
        {
            get;
            set;
        }

        public ICollection<Inventario.ResiduoAlmacen> Almacenes 
        {
            get;
            set;
        }
        public ICollection<Servicio.BitacoraResiduo> Bitacoras
        {
            get;
            set;
        }

    }
}
