﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Modelo.Servicio
{
    public class BitacoraResiduo
    {
        public int Id
        {
            get;
            set;
        }
        public int BitacoraId
        {
            get;
            set;
        }

        public Bitacora Bitacora
        {
            get;
            set;
        }
        public decimal Peso
        {
            get;
            set;
        }
        public decimal PesoProgramado
        {
            get;
            set;
        }

        public decimal MontoRecuperacion
        {
            get;
            set;
        }

        public decimal PorcentajeHumedad
        {
            get;
            set;
        }
        public decimal PorcentajePolvo
        {
            get;
            set;
        }
        public decimal PorcentajeSuciedad
        {
            get;
            set;
        }

    }
}
