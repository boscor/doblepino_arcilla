﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Modelo.Servicio
{
    public class ContratoResiduo 
    {
        public int Id
        {
            get;
            set;
        }

        public int ContratoId
        {
            get;
            set;
        }
        public Contrato Contrato
        {
            get;
            set;
        }

        public int ResiduoId
        {
            get;
            set;
        }
        public ContratoResiduo residuo
        {
            get;
            set;
        }
        public decimal PesoProgramado
        {
            get;
            set;
        }
        public decimal MontoRecuperacion
        {
            get;
            set;
        }
    }
}
