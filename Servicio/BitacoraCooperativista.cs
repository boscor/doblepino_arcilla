﻿using Modelo.Persona;
using System;
using System.ComponentModel.DataAnnotations;

namespace Modelo.Servicio
{
    public class BitacoraCooperativista
    {
        public int Id
        {
            get;
            set;
        }
        public int BitacoraId
        {
            get;
            set;
        }
        public Bitacora Bitacora
        {
            get;
            set;
        }
        public int CooperativistaId
        {
            get;
            set;
        }
        public Cooperativista Cooperativista
        {
            get;
            set;
        }
        

    }
}
