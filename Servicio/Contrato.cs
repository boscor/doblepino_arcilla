﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Modelo.Servicio
{
    public class Contrato : Model
    {
        public decimal CostoEvento
        {
            get;
            set;
        }
        public decimal CostoEventoExtraordinario
        {
            get;
            set;
        }
        public int EventosporMes
        {
            get;
            set;
        }
        
        public int ClienteId
        {
            get;
            set;
        }
        public Cliente.Cliente Cliente
        {
            get;
            set;
        }

        public int DiasCredito
        {
            get;
            set;
        }


    }
}
