﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Modelo.Servicio
{
    public class Bitacora
    {
        public int Id
        {
            get;
            set;
        }
        [DataType(DataType.DateTime)]
        public DateTime Fecha
        {
            get;
            set;
        }
        [DataType(DataType.DateTime)]
        public DateTime FechaCaptura
        {
            get;
            set;
        }
        [Required]
        public string Observacion
        {
            get;
            set;
        }

        public ICollection<Servicio.BitacoraCooperativista> Cooperativistas
        {
            get;
            set;
        }

        public ICollection<BitacoraResiduo> Contratos
        {
            get;
            set;
        }



    }
}
