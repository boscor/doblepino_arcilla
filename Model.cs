﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
namespace Modelo
{
    public class Model
    {
        public int Id
        {
            get;
            set;
        }
        [Required, MaxLength(100)]
        public string Nombre
        {
            get;
            set;
        }
        [MaxLength(500)]
        public string Observacion
        {
            get;
            set;
        }
    }
}